# JTensor

A virtual machine with Matlab-like scripting for Tensor-based math and code-
generator to export to Java, C++ and Python.


Basic
=====
* Add
* Substract
* Multiply 
* Inverse


Geometric
=========
* Line/Line intersection
* Line Plane intersection
* Point on plane test

Dual space gemoetry
===================


Decompositions 
==============
* SVD
* QR
* Cholesky
